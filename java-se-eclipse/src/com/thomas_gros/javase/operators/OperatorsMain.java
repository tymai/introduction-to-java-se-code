package com.thomas_gros.javase.operators;

public class OperatorsMain {
	public static void main(String[] args) {
		
		// https://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html
		
		// Précédence des opérateurs, cf table de précédence => https://docs.oracle.com/javase/tutorial/java/nutsandbolts/operators.html
		int res = 3 + 5 % 3 / 2 >> 12;
		int resWithParenthesis = (3 + ((5 % 3) / 2)) >> 12;
		
		int a = 3;
		int b = 3;
		// de droite a gauche pour les operateurs d'assignation
		a += b = 4;
		System.out.println(a); // 7
		System.out.println(b); // 4
		
		// division
		System.out.println(5 / 2);		
		System.out.println(5 / 2 * 1.0);
		System.out.println(5.0 / 2);
		
		// float test = 5.0 / 2;
		// double test = 5f / 2;
		
		// égalité
		// == sur types primitifs vérifie si les valeurs sont identiques
		System.out.println( 1 == 1); // true
		System.out.println( 1 == 2); // false
		
		// == sur des objets vérifie si les objets sont identiques
		String tmp = "hello";
		System.out.println("hello" == "hello"); // true. compiler tricks :-) voir javap
		System.out.println("hello" == ("he" + "llo")); // true. compiler tricks :-) voir javap aussi !
		System.out.println(new String("hello") == new String("hello")); // false
		
		// .equals sur les objets vérifie l'égalité 'naturelle' des objets
		System.out.println("hello".equals("hello")); // true
		System.out.println("hello".equals(new String("hello"))); // true
		System.out.println((new String("hello")).equals(new String("hello"))); // true
				
		// shifts
		System.out.println(1 << 1); // 2      00000001 << 1 = 00000010
		System.out.println(1 << 2); // 4      00000001 << 2 = 00000100
		System.out.println(1 << 3); // 8      00000001 << 3 = 00001000
		System.out.println(2 << 2); // 8      00000010 << 2 = 00001000
	
		
	}

}
