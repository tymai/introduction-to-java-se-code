package com.thomas_gros.javase.designpatterns.singleton;

public class SingletonMain {

    public static void main(String[] args) {

        SingletonNonThreadSafe s1 = SingletonNonThreadSafe.newInstance();
        SingletonNonThreadSafe s2 = SingletonNonThreadSafe.newInstance();
        
        System.out.println(s1 == s2); // doit être true

        // System.out.println(SingletonThreadSafe.instance == SingletonThreadSafe.instance);
        System.out.println(SingletonThreadSafe.getInstance() == SingletonThreadSafe.getInstance());
    }

}
