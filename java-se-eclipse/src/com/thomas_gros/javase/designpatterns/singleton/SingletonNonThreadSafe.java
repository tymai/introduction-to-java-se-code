package com.thomas_gros.javase.designpatterns.singleton;

public class SingletonNonThreadSafe {
    
    private static SingletonNonThreadSafe instance;
    
    private SingletonNonThreadSafe() {    
    }
    
    public static SingletonNonThreadSafe newInstance() {
        if(instance == null) {
            instance = new SingletonNonThreadSafe();
        }
        
        return instance;
    }
}
