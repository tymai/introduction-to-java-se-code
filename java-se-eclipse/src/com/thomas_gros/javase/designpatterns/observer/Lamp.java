package com.thomas_gros.javase.designpatterns.observer;

public class Lamp {

    private boolean on = false;

    private Captor[] captors = new Captor[50];
    
    public void registerCaptor(Captor c) {
        for (int i = 0; i < captors.length; i++) {
            if(captors[i] == null) {
                captors[i] = c;
                break;
            }
        }
        
    }

    public boolean isOn() {
        return on;
    }
    
    public void setOn(boolean on) {
        this.on = on;
        for (int i = 0; i < captors.length; i++) {
            if(captors[i] == null) { break; }
            
            captors[i].notify(this);
        }
        
    }

}
