package com.thomas_gros.javase.designpatterns.observer;

public class CaptorType2 implements Captor {

    @Override
    public void notify(Lamp lamp) {
        System.out.println(this +" " + lamp + " " + lamp.isOn());
    }

}
