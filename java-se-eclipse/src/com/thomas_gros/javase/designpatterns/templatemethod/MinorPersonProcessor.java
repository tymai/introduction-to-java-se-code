package com.thomas_gros.javase.designpatterns.templatemethod;

public class MinorPersonProcessor extends PersonProcessor {

    @Override
    protected Person[] filter(Person[] persons) {
        System.out.println("filtrer les mineurs");
        return persons;
    }

    @Override
    protected void process(Person[] filteredPersons) {
        System.out.println("traiter les mineurs");
    }

    
    
}
