package com.thomas_gros.javase.designpatterns.templatemethod;

public class PersonProcessorMain {

    public static void main(String[] args) {
        
        Person[] persons = { 
                new Person("thomas", 20), 
                new Person("bob", 35), 
                new Person("tim", 3) 
                };
        
        PersonProcessor p = new AdultPersonProcessor();
        p.processPersons(persons);
        
        p = new MinorPersonProcessor();
        p.processPersons(persons);
    }

}
