package com.thomas_gros.javase.test;

public class Calculator {

    /**
     * a + b ne doit pas être supérieur à Integer.MAX_VALUE 
     * @param a
     * @param b
     * @return
     * @throws CalculatorException if a + b > Integer.MAX_VALUE
     */
    // ou alors IllegalArgumentException
    public int add(int a, int b) {
        // TODO jeter une exception si a + b > Integer.MAX_VALUE
        return a + b;
    }
    
}
