package com.thomas_gros.javase.javafx;

import java.io.IOException;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Browser extends Application {

    @Override
    public void start(Stage primaryStage) throws Exception {
        VBox vBox = FXMLLoader.load(Browser.class.getResource("browser.fxml"));
        Scene scene = new Scene(vBox);
        
        primaryStage.setScene(scene);
        primaryStage.show();
    }


}
