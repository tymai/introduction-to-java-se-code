package com.thomas_gros.javase.exception;

public class NetworkAccessException extends RuntimeException {

    public NetworkAccessException() {
        super();
        // TODO Auto-generated constructor stub
    }

    public NetworkAccessException(String message, Throwable cause, boolean enableSuppression,
            boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
        // TODO Auto-generated constructor stub
    }

    public NetworkAccessException(String message, Throwable cause) {
        super(message, cause);
    }

    public NetworkAccessException(String message) {
        super(message);
    }

    public NetworkAccessException(Throwable cause) {
        super(cause);
        // TODO Auto-generated constructor stub
    }

}
