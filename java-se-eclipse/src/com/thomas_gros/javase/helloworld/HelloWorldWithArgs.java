package com.thomas_gros.javase.helloworld;

public class HelloWorldWithArgs {

	public static void main(String[] args) {
		if(args.length == 1) {
			System.out.println("Hello, " + args[1] + " !");
		} else {
			System.out.println("You must specify one argument");
		}
	}
}
