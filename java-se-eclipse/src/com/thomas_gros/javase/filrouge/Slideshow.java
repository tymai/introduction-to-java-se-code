package com.thomas_gros.javase.filrouge;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Objects;

public class Slideshow implements Serializable {

    private static final long serialVersionUID = 1L;
    
    private ArrayList<Slide> slides;
    
    public Slideshow() {
        this.slides = new ArrayList<>();
    }

    /**
     * @throws NullPointerException if slide is null
     */
    public void addSlide(Slide slide) {
        Objects.requireNonNull(slide, "slide must not be null");
        slides.add(slide);
    }

    /**
     * @throws NullPointerException if slide is null
     */
    public void removeSlide(Slide slide) {
        Objects.requireNonNull(slide, "slide must not be null");
        slides.remove(slide);
    }
    
    public int size() {
        return slides.size();
    }
    
    public Slide[] getSlides() {
       return slides.toArray(new Slide[0]);
    }

    /**
     * @throws IndexOutOfBoundsException if index is < 0 or >= number of slides
     */
    private void rangeCheck(int index) {
        if(index < 0 || index >= slides.size()) {
            throw new IndexOutOfBoundsException(
                    String.format("Index: %d, Size: %d", index, slides.size()));
        }
    }

    /**
     * @throws IndexOutOfBoundsException if currentSlideIndex is < 0 or >= number of slides
     */
    public Slide getSlideAt(int currentSlideIndex) {
        rangeCheck(currentSlideIndex);

        return slides.get(currentSlideIndex);
    }

    @Override
    public String toString() {
        return "Slideshow [slides=" + slides + "]";
    }

}
