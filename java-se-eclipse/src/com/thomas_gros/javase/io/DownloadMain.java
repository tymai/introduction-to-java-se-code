package com.thomas_gros.javase.io;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;

public class DownloadMain {

    public static final int CAPACITY = 8192;
    
    public static void main(String[] args) throws IOException {
        URL url = new URL("http://thomas-gros.com/trainings/introduction-a-java-se/index.html");
        
        InputStream is = url.openStream();
        BufferedInputStream bis = new BufferedInputStream(is);

        // sous windows c:\\test-data\\file.html
        FileOutputStream fos = new FileOutputStream("/Users/thomasgros/Documents/Courses/test-data/file.html");        
        BufferedOutputStream bos = new BufferedOutputStream(fos);
        
//      int b;
//      while((b = bis.read()) != -1) {
//          fos.write(b);
//      }
        
        byte[] data = new byte[CAPACITY];
        
        int nbBytesRead;
        while((nbBytesRead = bis.read(data)) != -1) {
            // fos.write(data, 0, nbBytesRead);
            bos.write(data, 0, nbBytesRead);
        }
        
        bis.close();
        bos.close();
    }

}
