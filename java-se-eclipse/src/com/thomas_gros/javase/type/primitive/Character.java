package com.thomas_gros.javase.type.primitive;

public class Character {

	public static void main(String[] args) {
		// char
		// single 16-Bit Unicode character
		// valeurs entre \u0000 (0) et \uffff (65,535)
		// par defaut \u0000
		
		char myChar = 'a';
		char myChar2 = '\u265E';
		
		System.out.println(myChar2);

	}

}
