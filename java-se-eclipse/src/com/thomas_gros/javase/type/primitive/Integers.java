package com.thomas_gros.javase.type.primitive;

public class Integers {

	public static void main(String[] args) {	
		// byte
		// 8-bit signed two's complement integer
		// -128 .. +127
		// par default 0
		byte myByte = 12;
		
		
		// byte myOtherByte = 12345; // mismatch cannot convert from int to byte
		
		// short
		// 16-bit signed two's complement integer
		// -32,768 .. +32767
		// par default 0
		short myShort = 1234;
		
		// int
		// 32-bit signed two's complement integer
		// -2^31 .. +2ˆ31-1
		// depuis Java 8 il est possible de les utiliser comme entiers signés 0.. 2ˆ32-1
		// par default 0
		int myInt = 12345;
		int myOtherInt = 0x1A;
		int MyOtherOtherInt = 0b101;
		int myInt4 = 123_456_789;
		int myInt5 = 0b1101_1100_0110;
		int myInt6 = 0xCAFE_BABE; // debut de tous les fichiers .class 
				
		// long
		// 64-bit signed two's complement integer
		// -2^63 .. +2ˆ63-1
		// depuis Java 8 il est possible de les utiliser comme long signés 0.. 2ˆ64-1
		// par default 0L
		long myLong = 123456;
		long myLong2 = 123456L;

		
		

	}

}
