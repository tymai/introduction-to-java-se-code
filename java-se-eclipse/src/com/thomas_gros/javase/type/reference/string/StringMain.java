package com.thomas_gros.javase.type.reference.string;

public class StringMain {

	public static void main(String[] args) {

		String myString = "a string"; // literal

		char[] myStringArray = { 'a', ' ', 's', 't', 'r', 'i', 'n', 'g' };
		String myStringFromCharArray = new String(myStringArray); // via un
																	// constructeur
		// String myStringFromCharArray = new String({ 'a', ' ', 's', 't', 'r',
		// 'i', 'n', 'g' });

		System.out.println(myString);

		System.out.println(myString.length()); // longueur de la String
		System.out.println(myString.isEmpty()); // est-ce que la String est vide
												// ? false
		System.out.println("".isEmpty()); // true

		System.out.println(myString.charAt(2)); // retourne le caractère à la
												// position 2

		System.out.println(myString.toUpperCase()); // retourne une nouvelle
													// String en majuscule

		// Strings sont immutables !!!
		// concatenation
		String hello = "hello";
		String world = "world";

		String msg = hello + world;
		System.out.println(msg);

		String msg2 = hello + " Thomas";
		System.out.println(hello);
		System.out.println(msg2);

		System.out.println(hello.concat(" Bob"));
		System.out.println(hello);

		String avoid = "Bonjour" + "je" + "suis" + "une" + "string" + "!";

		// StringBuilder (Pas Thread Safe) / StringBuffer (Thread Safe) pour
		// construction plus efficace de Strings
		StringBuilder sb = new StringBuilder();
		sb.append("Bonjour");
		sb.append(" ");
		sb.append("je");
		sb.append(" ");
		sb.append("suis");
		sb.append(" ");
		sb.append(" une string !");
		String result = sb.toString();

	}

}
