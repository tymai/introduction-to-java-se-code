package com.thomas_gros.javase.serialization;

import java.io.Serializable;

public class MyClass implements Serializable {

    /**
     * 
     */
    private static final long serialVersionUID = 1L;
    
    transient private String s;
    private int i;
    
    public MyClass(String s, int i) {
        super();
        this.s = s;
        this.i = i;
    }

    @Override
    public String toString() {
        return "MyClass [s=" + s + ", i=" + i + "]";
    }

}
