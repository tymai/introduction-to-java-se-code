package com.thomas_gros.javase.collections.list;

import java.util.Comparator;

import com.thomas_gros.javase.collections.Person;

public class PersonSSNComparator implements Comparator<Person> {
    // plus tard nous utiliserons des enums ici
    public static final int DESC = -1;
    public static final int ASC = 1;
    
    private int sortOrder;
    
    public PersonSSNComparator(int sortOrder) {
        if(sortOrder == 0) { throw new IllegalArgumentException("sortOrder can't be 0"); }
        this.sortOrder = sortOrder;
    }
    
    @Override
    public int compare(Person p1, Person p2) {
        return sortOrder * p1.getSsn().compareTo(p2.getSsn());

    }
    
}
