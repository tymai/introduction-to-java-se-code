package com.thomas_gros.javase.oop;

public class MainPerson {

	public static void main(String[] args) {
		
		Person thomas = new Person();
		thomas.setFirstName("Thomas");
		thomas.setLastName("Gros");
		
		Person bob = new Person();
		bob.setFirstName("Bob");
		bob.setLastName("Dylan");
		
	}
}
