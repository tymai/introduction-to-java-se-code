package com.thomas_gros.javase.oop.inheritance;

public class InheritanceMain {

    public static void main(String[] args) {
        SubClass sub = new SubClass();
        System.out.println(sub.aPublicField);
        System.out.println(sub.aProtectedField);
        System.out.println(sub.aPackagePrivateField); 
        
        sub.aProtectedMethod();
        sub.aPublicMethod();
    }

}
