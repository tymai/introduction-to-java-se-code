package com.thomas_gros.javase.oop.inheritance;

public class SuperClass {
    private String aPrivateField = "private field";
    String aPackagePrivateField = "package private field";
    protected String aProtectedField = "protected field";
    public String aPublicField = "public field";
    
    private void aPrivateMethod() {}
    protected void aProtectedMethod() {}
    public void aPublicMethod() {}
}
