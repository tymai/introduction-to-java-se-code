package com.thomas_gros.javase.oop.inheritance;

/**
 * Une sous classe hérite de tous les membres de la super class.
 * Les membres (fields / methods) peuvent-être utilisés normalement, remplacés,
 * cachés, complétés par de nouveaux membres.
 * 
 * Les constructeurs ne sont pas des membres, ils ne sont pas hérités.
 * 
 * Il n'est possible d'hériter que d'une seule classe.
 * 
 * Toutes les classes héritent de Object si aucune super class n'est explicitement étendue. 
 * 
 * @author thomasgros
 *
 */
public class SubClass extends SuperClass {

}
