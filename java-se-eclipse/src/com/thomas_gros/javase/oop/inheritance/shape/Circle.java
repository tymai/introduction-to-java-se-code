package com.thomas_gros.javase.oop.inheritance.shape;

public class Circle extends Shape {

    private int radius;

    public Circle() {
        this(0,0,0);
    }
    
    public Circle(int x, int y, int radius) {
        super(x, y);
        this.radius = radius;
    }

    public int getRadius() {
        return radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }
    
}
