package com.thomas_gros.javase.oop.inheritance;

public class Dog extends Animal {
    public Dog() {
        // appel du constructeur de la classe parent. 
        // présent dans le constructeur par défaut généré par le compilateur 
        super();
        System.out.println("Constructeur de Dog");
    }
    
    @Override
    public String toString() {
        return "I am a dog";
    }
    
    @Override
    public void makeSomeSound() {
        System.out.println("Wooof");
        
    }
}