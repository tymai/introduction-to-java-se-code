package com.thomas_gros.javase.oop.inheritance;

//Impossible de créer une instance de Animal qui est abstract
public abstract class Animal {

    private int age;
    
    public Animal() {
        super(); // appelle le contructeur de la super class
        System.out.println("Constructeur de Animal");
    }

    public Animal(int age) {
        this.age = age;
    }

    public int getAge() {
        return age;
    }
    
    public void setAge(int age) {
        this.age = age;
    }
    
    public abstract void makeSomeSound();
    
}
