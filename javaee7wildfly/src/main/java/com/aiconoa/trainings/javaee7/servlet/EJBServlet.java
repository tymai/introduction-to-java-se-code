package com.aiconoa.trainings.javaee7.servlet;

import java.io.IOException;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aiconoa.trainings.javaee7.ejb.SingletonEJB;
import com.aiconoa.trainings.javaee7.ejb.StatefulEJB;
import com.aiconoa.trainings.javaee7.ejb.StatelessEJB;

/**
 * Servlet implementation class EJBServlet
 */
@WebServlet("/EJBServlet")
public class EJBServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOGGER = Logger.getLogger(EJBServlet.class.getName());

	// @EJB AVANT QUE CDI N'EXISTE
	@Inject
	private StatelessEJB statelessEJB;
	
	@Inject
	private StatefulEJB statefulEJB;

	@Inject
	private SingletonEJB singletonEJB;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	    LOGGER.info("###################################");
	    
	    // EJB stateless => rien ne garanti que la même instance d'un EJB sera 
	    // utilisée entre les deux lignes suivantes
	    LOGGER.info(String.format("statelessEJB.getValue(): %f", statelessEJB.getValue()));
	    LOGGER.info(String.format("statelessEJB.getValue(): %f", statelessEJB.getValue()));
	    
	    // EJB stateful => les deux lignes suivantes utiliseront la même instance
	    // d'un EJB stateful. Une même instance du client (ici servlet)  'garde'
	    // la même instance d'un EJB stateful.
	    LOGGER.info(String.format("statefulEJB.getValue(): %f", statefulEJB.getValue()));
	    LOGGER.info(String.format("statefulEJB.getValue(): %f", statefulEJB.getValue()));
	    
	    // EJB Singleton => toujours la même instance retournée quelque soit le client
	    LOGGER.info(String.format("singletonEJB.getValue(): %f", singletonEJB.getValue()));
	    LOGGER.info(String.format("singletonEJB.getValue(): %f", singletonEJB.getValue()));
	    LOGGER.info("###################################");
	    
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}


}
