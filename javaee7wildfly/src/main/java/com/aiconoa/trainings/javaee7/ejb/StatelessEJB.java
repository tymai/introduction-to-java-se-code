package com.aiconoa.trainings.javaee7.ejb;

import javax.ejb.Stateless;

/**
 * Session Bean implementation class StatelessEJB
 */
@Stateless
public class StatelessEJB {
    
    private double value = Math.random();
    
    public double getValue() {
        return value;
    }

}
