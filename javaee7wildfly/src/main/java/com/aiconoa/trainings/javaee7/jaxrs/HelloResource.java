package com.aiconoa.trainings.javaee7.jaxrs;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

// http://localhost:8080/javaee7/sakila-rest-api/hello
@Path("/hello")
public class HelloResource {

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String helloJSON() {
        return "{ \"data\" : \"hello\" }";
    }
    
    @GET
    @Produces(MediaType.APPLICATION_XML)
    public String helloXML() {
        return "<data>hello</data>";
    }
    
}
