package com.aiconoa.trainings.javaee7.slideshow.servlet;

import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.aiconoa.trainings.javaee7.servlet.filter.LogFilter;
import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.ImageSlideElement;
import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.Slide;
import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.SlideElement;
import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.Slideshow;
import com.aiconoa.trainings.javaee7.slideshow.model.nonjpa.TextSlideElement;
import com.aiconoa.trainings.javaee7.slideshow.service.SlideshowService;

@WebServlet("slideshow")
public class SlideshowServlet extends HttpServlet {
    private static final Logger LOGGER = Logger.getLogger(SlideshowServlet.class.getName());
    
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        LOGGER.info("SlideshowServlet - doGet - " + req.getQueryString());
        
        SlideshowService slideshowService = new SlideshowService();

        String slideshowIdParam = req.getParameter("id");
        
        if(slideshowIdParam == null) {           
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "la page demandée n'existe pas");
            return;
        }
        
        
        if(! slideshowIdParam.matches("\\d+")) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "id doit être un entier");
            return;
        }
        
        int slideshowId = Integer.parseInt(slideshowIdParam);
        
        Slideshow slideshow = slideshowService.findSlideshow(slideshowId);
        if(slideshow == null) {           
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "la page demandée n'existe pas");
            return;
        }   
        
        String slideIndexParam = req.getParameter("index");
        if(slideIndexParam == null) {           
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "la page demandée n'existe pas");
            return;
        }
        
        
        if(! slideIndexParam.matches("\\d+")) {
            resp.sendError(HttpServletResponse.SC_BAD_REQUEST, "index doit être un entier");
            return;
        }
        
        int slideIndex = Integer.parseInt(slideIndexParam);
        
        
        if(slideIndex < 0 || slideIndex >= slideshow.size()) {           
            resp.sendError(HttpServletResponse.SC_NOT_FOUND, "la page demandée n'existe pas");
            return;
        }
        
        Slide slide = slideshow.getSlideAt(slideIndex);
        
        resp.setCharacterEncoding("utf-8");
        resp.addHeader("Content-Type", "text/html");
        
        resp.getWriter().append("<!DOCTYPE html>")
                        .append("<html>")
                            .append("<head>")
                                .append("<meta charset=\"utf-8\">")
                                .append("<title>Coucou</title>")
                                .append("<link rel=\"stylesheet\" href=\"slideshow/css/reset.css\">")
                                .append("<link rel=\"stylesheet\" href=\"slideshow/css/slideshow.css\">")
                            .append("</head>")
                            .append("<body>")

                            .append("<div class=\"container\">")
                            .append("<div class=\"slideshow\">")
                            .append(String.format("<h1 class=\"slideshow__title\">%s</h1>", slideshow.getTitle()))
                            .append(
//                                    String.format("<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" class=\"slideshow__slide\" width=\"%d\" height=\"%d\" viewport=\"0 0 %d %d\" xmlns:xlink= \"http://www.w3.org/1999/xlink\">", slideshow.getWidth(), slideshow.getHeight(), slideshow.getWidth(), slideshow.getHeight()));
                                    String.format("<svg xmlns=\"http://www.w3.org/2000/svg\" version=\"1.1\" class=\"slideshow__slide\" viewport=\"0 0 %d %d\" xmlns:xlink= \"http://www.w3.org/1999/xlink\">", slideshow.getWidth(), slideshow.getHeight()));
    
        
        for(SlideElement<?> slideElement : slide.getSlideElements()) {
            if(slideElement instanceof TextSlideElement) {
                // nous faisons un raccourci en décidant que le texte est aligné au milieu dans sa BBox, http://apike.ca/prog_svg_text_style.html
                resp.getWriter()
                    .append(String.format("<text text-anchor=\"middle\" alignment-baseline=\"middle\" x=\"%f\"  y=\"%f\" font-family=\"Helvetica\" font-size=\"40\"  fill=\"black\">%s</text>", slideElement.getX(), slideElement.getY(), slideElement.getContent()));
            } else if(slideElement instanceof ImageSlideElement) {
                resp.getWriter()
                    .append(String.format("<image xlink:href=\"/javaee7/img?img=%s\" x=\"%f\" y=\"%f\" width=\"%f\" height=\"%f\" preserveAspectRatio=\"xMidYMid meet\"/>", slideElement.getContent(), slideElement.getX(), slideElement.getY(), slideElement.getWidth(), slideElement.getHeight()));               
            }
        }
          
    resp.getWriter()                
                            .append("</svg>")
                            .append("<div class=\"slideshow__hud\">");
    
    String previousLinkClasses = "slideshow__hud__link";
    if(slideIndex <= 0) {
        previousLinkClasses += " slideshow__hud__link--hidden";
    }
    resp.getWriter().append(String.format("<a class=\"%s\" href=\"slideshow?id=123&index=%d\">previous</a>", previousLinkClasses, slideIndex - 1));
    
    String nextLinkClasses = "slideshow__hud__link";
    if(slideIndex >= slideshow.size() - 1) {
        nextLinkClasses += " slideshow__hud__link--hidden";
    }
    resp.getWriter().append(String.format("<a class=\"%s\" href=\"slideshow?id=123&index=%d\">next</a>", nextLinkClasses, slideIndex + 1));
                              
    resp.getWriter().append(String.format("<p class=\"slideshow__hud__index\">%d/%d</p>", slideIndex + 1, slideshow.size()))
                                .append("</div>")
                                .append("</div>")
                                .append("</div>")
                            .append("</body>")
                        .append("</html>");
    }
    
}
