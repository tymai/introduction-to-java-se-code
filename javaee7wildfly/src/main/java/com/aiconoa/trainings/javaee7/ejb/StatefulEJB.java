package com.aiconoa.trainings.javaee7.ejb;

import javax.ejb.Stateful;

/**
 * Session Bean implementation class StatefulEJB
 */
@Stateful
public class StatefulEJB {

    private double value = Math.random();
    
    public double getValue() {
        return value;
    }

}
