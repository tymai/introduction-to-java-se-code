package com.aiconoa.trainings.javaee7.slideshow.jaxrs;

import java.util.List;
import java.util.logging.Logger;

import javax.inject.Inject;
import javax.json.Json;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObjectBuilder;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.NotFoundException;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import com.aiconoa.trainings.javaee7.slideshow.model.jpa.ImageSlideElement;
import com.aiconoa.trainings.javaee7.slideshow.model.jpa.Slide;
import com.aiconoa.trainings.javaee7.slideshow.model.jpa.SlideElement;
import com.aiconoa.trainings.javaee7.slideshow.model.jpa.Slideshow;
import com.aiconoa.trainings.javaee7.slideshow.model.jpa.TextSlideElement;
import com.aiconoa.trainings.javaee7.slideshow.service.SlideshowServiceEJB;

@Path("/slideshows")
public class SlideshowResource {
    
    private static final Logger LOGGER = Logger.getLogger(SlideshowResource.class.getName());

    @Inject
    private SlideshowServiceEJB slideshowService;
    
//    Voir ReastEasy (implémentation JAX-RS au sein de WildFly) pour (un) marshalling
//    http://docs.jboss.org/resteasy/docs/3.0.19.Final/userguide/html_single/index.html#Content_Marshalling_Providers
    
//    Voir http://www.jsonschema2pojo.org pour une aide au marshalling    
    
//    JAX-RS
//    type retour -----  MessageBodyWriter    -------> Response
//    List<Slideshow> | JSON
//    String | JSON
//    List<Slideshow> | HTML
    
    // JAXB (Jackson) Object <---> XML
    // (Jackson) Object <-> JSON  (JAJB prévu dans Java EE 8)
    
//    @GET
//    @Produces(MediaType.APPLICATION_JSON)
//    public List<Slideshow> getSlideshows() {
//        
//        List<Slideshow> slideshows = slideshowService.findAllSlideshowWithSlidesAndSlideElements();
//        
//        return slideshows;
//    }
    
    
//  @GET
//  @Produces(MediaType.APPLICATION_JSON)
//  public Response getSlideshows() {
//      List<Slideshow> slideshows = slideshowService.findAllSlideshowWithSlidesAndSlideElements();
//      
//      JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
//      for (Slideshow slideshow : slideshows) {
//          jsonArrayBuilder.add(slideshowToJsonObjectBuilder(slideshow));
//      }
//      
////      return jsonArrayBuilder.build().toString();
//      return Response.ok(jsonArrayBuilder).build();
//  }
    
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getSlideshows() {
        List<Slideshow> slideshows = slideshowService.findAllSlideshowWithSlidesAndSlideElements();
        
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();
        for (Slideshow slideshow : slideshows) {
            jsonArrayBuilder.add(slideshowToJsonObjectBuilder(slideshow));
        }
        
        return jsonArrayBuilder.build().toString();
    }
    
    @POST
    @Consumes(MediaType.APPLICATION_JSON) // le Content-Type de la requête est application/json
    public String postSlideshow(Slideshow slideshow) {
        LOGGER.info(slideshow.toString());
        
        slideshowService.createSlideshow(slideshow);
        
        return "{ \"url\": \"/slideshows/slideshow.getId()\"}";
    }
    
//    @POST
//    @Path("/{id}/slides") // POST /slideshow-api/slideshows/1/slides
//    @Consumes(MediaType.APPLICATION_JSON)
//    public String postSlide(Integer slideshowId, Slide slide) {
//        slideshowService.createSlide(slideshowId, slide);
//    }
    
    
    
    @GET
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public String getSlideshow(@PathParam("id") Integer id) {
        Slideshow slideshow = slideshowService.findSlideshow(id);
        
        if(slideshow == null) {
            throw new NotFoundException();
        }
        
        return slideshowToJsonObjectBuilder(slideshow).build().toString();
    }

    private JsonObjectBuilder slideshowToJsonObjectBuilder(Slideshow slideshow) {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        jsonObjectBuilder.add("id", slideshow.getId());
        jsonObjectBuilder.add("title", slideshow.getTitle());
        jsonObjectBuilder.add("width", slideshow.getWidth());
        jsonObjectBuilder.add("height", slideshow.getHeight());
        
        JsonArrayBuilder jsonSlidesArrayBuilder = Json.createArrayBuilder();
        for (Slide slide : slideshow.getSlides()) {
            jsonSlidesArrayBuilder.add(slidetoJsonObjectBuilder(slide));
        }
        
        jsonObjectBuilder.add("slides", jsonSlidesArrayBuilder);
        
        return jsonObjectBuilder;
    }
    
    private JsonObjectBuilder slidetoJsonObjectBuilder(Slide slide) {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        
        jsonObjectBuilder.add("id", slide.getId());
        jsonObjectBuilder.add("position", slide.getPosition());
        jsonObjectBuilder.add("authorComment", slide.getAuthorComment());
        
        
        JsonArrayBuilder jsonSlidesArrayBuilder = Json.createArrayBuilder();
        for (SlideElement<?> slideElement : slide.getSlideElements()) {
            jsonSlidesArrayBuilder.add(slideElementToJsonObjectBuilder(slideElement));
        }
        
        jsonObjectBuilder.add("slideElements", jsonSlidesArrayBuilder);
        
        
        return jsonObjectBuilder;
    }
    
    private JsonObjectBuilder slideElementToJsonObjectBuilder(SlideElement<?> slideElement) {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        
        jsonObjectBuilder.add("x", slideElement.getX());
        jsonObjectBuilder.add("y", slideElement.getY());
        jsonObjectBuilder.add("width", slideElement.getWidth());
        jsonObjectBuilder.add("height", slideElement.getHeight());
        jsonObjectBuilder.add("content", slideElement.getContent().toString());

        if(slideElement instanceof TextSlideElement) {
            jsonObjectBuilder.add("type", "TextSlideElement");    
        } else if (slideElement instanceof ImageSlideElement) {
            jsonObjectBuilder.add("type", "ImageSlideElement");
        } else {
            // TODO log, jetter exception ?
            jsonObjectBuilder.add("type", "Type Error");
        }
        
        return jsonObjectBuilder;
    }
    
    
    
    
    
    
    
    
}
