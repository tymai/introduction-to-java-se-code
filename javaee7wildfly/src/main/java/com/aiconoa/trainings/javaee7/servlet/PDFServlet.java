package com.aiconoa.trainings.javaee7.servlet;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("pdf")
public class PDFServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.addHeader("Content-Type", "application/pdf");
        resp.addHeader("Content-Disposition", "attachement; filename=serlvet-spec.pdf");
        
        // pour un fichier qui est dans le .war
//        ServletContext context = getServletContext();
//        URL ressourceURL = context.getResource("/WEB-INF/myfile.pdf");
        
        FileInputStream fis = new FileInputStream("/Users/thomasgros/Desktop/servlet-3_1-final.pdf");
        BufferedInputStream bis = new BufferedInputStream(fis);
        
        byte[] buffer = new byte[8192];
        int length;
        while( (length = bis.read(buffer)) != -1) {
            resp.getOutputStream().write(buffer, 0, length);
        }
        
        bis.close();
        resp.getOutputStream().flush();

    }
    
}
