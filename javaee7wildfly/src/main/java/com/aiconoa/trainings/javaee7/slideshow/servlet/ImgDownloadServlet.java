package com.aiconoa.trainings.javaee7.slideshow.servlet;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.logging.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("img")
public class ImgDownloadServlet extends HttpServlet {
    
    private static final Logger LOGGER = Logger.getLogger(ImgDownloadServlet.class.getName());
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        String imgName = req.getParameter("img");
        
        // déduire un mime-type en adéquation avec le imgName
        resp.addHeader("Content-Type", "image/jpeg");

        // resp.addHeader("Content-Disposition", "attachement; filename=serlvet-spec.pdf");

        
        // ATTENTION GROSSE FAILLE DE SECURITE, BIEN SECURISER l'ACCES AU NOM DE FICHIER...
        String path = "/Users/thomasgros/Desktop/upload/" + imgName;
        FileInputStream fis = new FileInputStream(path);
        LOGGER.info("asked to download image at " + path);
        
        BufferedInputStream bis = new BufferedInputStream(fis);
        
        byte[] buffer = new byte[8192];
        int length;
        while( (length = bis.read(buffer)) != -1) {
            resp.getOutputStream().write(buffer, 0, length);
        }
        
        bis.close();
        resp.getOutputStream().flush();

    }
    
}
