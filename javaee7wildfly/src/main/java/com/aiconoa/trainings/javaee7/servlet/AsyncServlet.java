package com.aiconoa.trainings.javaee7.servlet;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.servlet.AsyncContext;
import javax.servlet.AsyncEvent;
import javax.servlet.AsyncListener;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(asyncSupported = true, urlPatterns = "async")
public class AsyncServlet extends HttpServlet {
    private static final Logger logger = Logger.getLogger(AsyncServlet.class.getName());

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        logger.info("before start async");
        
        AsyncContext context = req.startAsync();

        context.start(() -> {
           logger.info("the async work"); 
           try {
               Thread.sleep(5000);
               resp.getWriter().print("done !");
           } catch (Exception ex) {
               logger.log(Level.INFO, "error processing response", ex);
           }
           context.complete();
        });
        
        logger.info("after start async");
    }
}
