package com.aiconoa.trainings.javaee7.jsf;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import com.aiconoa.trainings.javaee7.ejb.FilmService;
import com.aiconoa.trainings.javaee7.entity.Film;

@Named
@RequestScoped
public class FilmDetailBean {
    
    @Inject
    private FilmService filmService;
    
    private Integer id;

    private Film film;
    
    public Integer getId() {
        return id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    
    public void loadFilm() {
        this.film = filmService.findById(this.id);
    }
    
    public Film getFilm() {
        return film;
    }
    
}
