package com.aiconoa.trainings.javaee7.servlet;

import java.io.IOException;
import java.nio.charset.StandardCharsets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("hello")
public class HelloServlet extends HttpServlet {
    
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        // comment récupérer la queryString dans une Servlet
        // req.getQueryString(); // key=value&key2=value2&key3=value3
        String name = req.getParameter("name");
        
        // checker le header Accept présent dans le request

        // Préparer une réponse
        // 1) écrire les entêtes
        resp.setCharacterEncoding("utf-8");
        resp.addHeader("Content-Type", "text/html");
        
        // 2) écrire le body        
        // resp.getWriter() // text
        //resp.getOutputStream() // bytes
        resp.getWriter().append("<!DOCTYPE html>")
                        .append("<html>")
                            .append("<head>")
                                .append("<meta charset=\"utf-8\">") // dans les 1024 premier bytes
                                .append("<title>Coucou</title>")
                            .append("</head>")
                            .append("<body>")
                                .append("<p>")
                                .append("coucou ")
                                .append(name)
                                .append("</p>")
                            .append("</body>")
                        .append("</html>");
    }
    
}
