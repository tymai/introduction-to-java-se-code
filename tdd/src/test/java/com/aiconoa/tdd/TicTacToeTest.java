package com.aiconoa.tdd;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;


public class TicTacToeTest {
    
    TicTacToe ticTacToe;
    
    @Before
    public void setUp() {
        ticTacToe = new TicTacToe();
    }
    
    // Quand une pièce est placée hors de l’axe des X 
    // alors une exception RuntimeException est jetée
    
    @Test(expected=RuntimeException.class)
    // public void shouldThrowExceptionWhenXOutsideBoard
    public void whenXOutsideBoardTooSmallThenRuntimeException() {
        ticTacToe.play(0, 2);
    }
    
    @Test(expected=RuntimeException.class)
    public void whenXOutsideBoardTooBigThenRuntimeException() {
        ticTacToe.play(5, 2);
    }
    
    // Quand une pièce  est placée hors de l’axe des Y 
    // alors une exception RuntimeException est jetée
    @Test(expected=RuntimeException.class)
    public void whenYOutsideBoardTooSmallThenRuntimeException() {
        ticTacToe.play(2, 0);
    }
    
    @Test(expected=RuntimeException.class)
    public void whenYOutsideBoardTooBigThenRuntimeException() {
        ticTacToe.play(2, 5);
    }
    
    // Quand une pièce est placée sur une case non vide 
    // alors une exception RuntimeException est jetée
    @Test(expected=RuntimeException.class)
    public void whenPlayInOccupiedSquareThenRuntimeException() {
        ticTacToe.play(1, 2);
        ticTacToe.play(1, 2);
    }

    @Test
    // shouldStartWithPlayerX
    public void givenFirstTurnWhenCurrentPlayerThenX() {
        assertEquals('X', ticTacToe.getCurrentPlayer());
    }
    
    @Test
    public void givenLastTurnWasXWhenCurrentPlayerThenO() {
        ticTacToe.play(1,1);
        assertEquals('O', ticTacToe.getCurrentPlayer());
    }
    
    @Test
    public void givenLastTurnWasOWhenCurrentPlayerThenX() {
        ticTacToe.play(1,1);
        ticTacToe.play(1,2);
        assertEquals('X', ticTacToe.getCurrentPlayer());
    }
    
    
    @Test
    public void whenPlayANonVictoriousMoveThenNoWinner() {
        ticTacToe.play(2, 3);
        
        assertFalse(ticTacToe.isGameOver());
        assertNull(ticTacToe.getWinner());
    }
    
    @Test
    public void whenPlayHorizontalVictoriousMoveThenWinner() {
        ticTacToe.play(1, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 1); // X
        ticTacToe.play(2, 2); // O
        
        ticTacToe.play(3, 1); // X
        
        assertTrue(ticTacToe.isGameOver());
        assertNotNull(ticTacToe.getWinner());
        assertEquals('X', ticTacToe.getWinner().charValue());
        
    }
    
    @Test
    public void whenPlayVerticalVictoriousMoveThenWinner() {

        ticTacToe.play(2, 1); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(3, 1); // X
        ticTacToe.play(1, 2); // O
        ticTacToe.play(2, 2); // X
        
        ticTacToe.play(1, 3); // O
        
        assertTrue(ticTacToe.isGameOver());
        assertNotNull(ticTacToe.getWinner());
        assertEquals('O', ticTacToe.getWinner().charValue());
        
    } 
    
    @Test
    public void whenPlayDiagonalBottomLeftTopRightVictoriousMoveThenWinner() {
        ticTacToe.play(1, 3); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(2, 2); // X
        ticTacToe.play(2, 1); // O
        ticTacToe.play(3, 1); // X
        
        assertTrue(ticTacToe.isGameOver());
        assertNotNull(ticTacToe.getWinner());
        assertEquals('X', ticTacToe.getWinner().charValue());
    }
    
    @Test
    public void whenPlayDiagonalTopLeftBottomRightVictoriousMoveThenWinner() {
        ticTacToe.play(1, 3); // X
        ticTacToe.play(1, 1); // O
        ticTacToe.play(1, 2); // X
        ticTacToe.play(2, 2); // O
        ticTacToe.play(2, 1); // X
        ticTacToe.play(3, 3); // O
        
        assertTrue(ticTacToe.isGameOver());
        assertNotNull(ticTacToe.getWinner());
        assertEquals('O', ticTacToe.getWinner().charValue()); 
    }
    
   @Test
   public void whenBoardIsFullWithoutVictoriousMoveThenNoWinner() {

       ticTacToe.play(1, 1); // X
       ticTacToe.play(2, 1); // O
       ticTacToe.play(3, 1); // X
       ticTacToe.play(1, 2); // O
       ticTacToe.play(1, 3); // X
       ticTacToe.play(2, 2); // O
       ticTacToe.play(3, 2); // X
       ticTacToe.play(3, 3); // O
       ticTacToe.play(2, 3); // X
       
       assertTrue(ticTacToe.isGameOver());
       assertTrue(ticTacToe.isDrawGame());
       assertNull(ticTacToe.getWinner());
   }
   
    
    
  
}
